'use strict';

export class DeferredPromise {
    resolve: (arg?: any) => void = null;
    reject:  (arg?: any) => void = null;
    promise: Promise<any>;
    
    constructor(value?: any) {
        this.promise = new Promise((resolveFn, rejectFn) => {
            this.resolve = resolveFn;
            this.reject  = rejectFn;
            
            if(value) {
                this.resolve(value);
            }
        });
    }
}