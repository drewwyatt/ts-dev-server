export interface WSServer {
    socket: WSSocket;
    connections: any[];
    domain: any;
}

export interface WSSocket {
}