const ws = require("nodejs-websocket")
import {WSServer} from "./ws.d.ts";

export class WS {
    private static _instance: WS;
    private _server;

    static get instance(): WS {
        if (!WS._instance) {
            WS._instance = new WS();
        }

        return WS._instance;
    }
    
    startServer(): WSServer {
        return ws.createServer((conn) => {
            console.log("New connection")
            
            this._makeNoise(conn);
            
            conn.on("text", function (str) {
                console.log("Received " + str)
                conn.sendText(str.toUpperCase() + "!!!")
            })
            conn.on("close", function (code, reason) {
                console.log("Connection closed")
            })
        }).listen(1337);
    }
    
    private _makeNoise(connection: any): void {
        setTimeout(() => { connection.sendText('Hey!'); this._makeNoise(connection) }, 2000);
    }
}