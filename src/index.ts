/// <reference path="../typings/main.d.ts" />
"use strict";

const app  = require("express")();
const http = require("http").Server(app);
import {WS} from './ws';

let env: any = {};

import {Routes} from "./routes";

app.set("port", (env.PORT || 8080));
app.get("/", Routes.index);

console.log(WS.instance.startServer());

http.listen(app.get("port"), () => {
  console.log("Node app is running at localhost:" + app.get("port"))
});
